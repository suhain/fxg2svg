from lxml import etree
from PIL import Image, ImageFont, ImageDraw
import logging
from fontTools import ttLib

from ..utils import process_transform, get_attribute
from ..utils.fonts import get_font, get_font_alias, get_font_file
from .default import DefaultTagProcessor


logger = logging.getLogger(__name__)


def get_recursive_data(element):
    text = element.text or ''
    for e in element:
        text += get_recursive_data(e)
    return text


def process_font(element, **attrs):
    params = {}
    if 'fontFamily' in attrs:
        params['font-family'] = attrs['fontFamily']

    if 'fontWeight' in attrs:
        params['font-weight'] = attrs['fontWeight']
    elif get_attribute(element, 'fontWeight'):
        params['font-weight'] = get_attribute(element, 'fontWeight')

    if 'fontStyle' in attrs:
        params['font-style'] = attrs['fontStyle']
    elif get_attribute(element, 'fontStyle'):
        params['font-style'] = get_attribute(element, 'fontStyle')

    if 'fontSize' in attrs:
        params['font-size'] = attrs['fontSize']
    if 'color' in attrs:
        params['fill'] = attrs['color']
    if 'textAlign' in attrs:
        params['text-anchor'] = {
            'left': 'start',
            'center': 'middle',
            'right': 'end',
        }[attrs['textAlign']]
    if 'lineHeight' in attrs:
        params['line-height'] = attrs['lineHeight']

    if 'font-family' in params:
        if 'font-weight' in params:
            params['font-family'] = "%s %s" % (params['font-family'].capitalize(), params['font-weight'].capitalize())
        if 'font-style' in params:
            params['font-family'] = "%s %s" % (params['font-family'].capitalize(), params['font-style'].capitalize())

    return params


processors = {}
logger = logging.getLogger(__name__)


def tag_processor(tagname):
    def register_tag_processor(cls):
        processors[tagname] = cls()
        return cls
    return register_tag_processor


@tag_processor('RichText')
class RichTextTagProcessor(DefaultTagProcessor):

    def on_start(self, target):
        target._p_count = -1
        params = {}
        params.update(process_transform(**target.attrs))
        params.update(process_font(target.element, **target.attrs))
        target.element = etree.SubElement(
            target.element,
            "text",
            **params
        )

    def on_end(self, target):
        family = target.element.get('font-family')
        size = target.element.get('font-size')
        text = get_recursive_data(target.element)
        if size is not None:
            size = float(size)
            font_object = get_font(family, fontdir=target.fontdir, size=round(size))
        else:
            font_object = get_font(family, fontdir=target.fontdir)
            fw, fh = font_object.getsize(text)
            size = fh
        asc, desc = font_object.getmetrics()
        fontpath = get_font_file(family, f='ttf', fontdir=target.fontdir)
        font = ttLib.TTFont(fontpath)
        sTypoAscender = font['OS/2'].sTypoAscender
        unitsPerEm = font['head'].unitsPerEm
        target.element.attrib['y'] = str(size*sTypoAscender/unitsPerEm)
        target.element.attrib['font-family'] = get_font_alias(font_object)
        target.element = target.element.getparent()
        logger.debug("Y offset for text: '%s' is %s" % (text, size - desc))


@tag_processor('p')
class PTagProcessor(DefaultTagProcessor):

    def on_start(self, target):
        target._p_count += 1
        params = {}
        params.update(process_font(target.element, **target.attrs))
        params['x'] = '0'
        dy = max([
            float(get_attribute(target.element, 'line-height') or '0'),
            float(get_attribute(target.element, 'font-size') or '0'),
        ])
        if target._p_count > 0:
            params['dy'] = str(dy)
        target.element = etree.SubElement(
            target.element,
            "tspan",
            **params
        )


@tag_processor('span')
class SpanTagProcessor(PTagProcessor):
    def on_start(self, target):
        params = {}
        params.update(process_font(target.element, **target.attrs))
        if 'font-family' in params:
            del params['font-family']
        target.element = etree.SubElement(
            target.element,
            "tspan",
            **params
        )

    def on_data(self, target):
        super(SpanTagProcessor, self).on_data(target)
        typographic_case = target.get_attribute('typographicCase')
        if typographic_case:
            if typographic_case == "uppercase":
                target.element.text = target.text.upper()
            if typographic_case == "lowercase":
                target.element.text = target.text.lower()

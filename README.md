# FXG to SVG converter #

Converter features:

* filters
* gradients
* shapes (path, line, rect, ellipse)
* text


## Installation ##

Install using pip

```
#!python

pip install fxg2svg
```

## Example ##

The package provides a console script to show how the conversion works

```
#!python

fxg2svg file.fxg [file.svg]
```